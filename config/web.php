<?php

$main = require(__DIR__ . '/main.php');

$web = [
    'name' => 'Студия ландшафтного дизайна Натальи Гондик',
    'bootstrap' => [
        'log',
    ],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'site/index',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'ZRpT_n7XW7X2r8Qd-VJ12mvue0WMaQHG',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'user' => [
            //'class' => 'app\extended\yii\web\User',
            //'identityClass' => 'app\models\Users',
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            //'enableStrictParsing'=>true,
            //'suffix' => '.html',
            'rules' => [
                '/' => 'site/index',
//                'gii' => 'gii',
                'departments' => 'departments/index',
                'units' => 'units/index',
                'users' => 'users/index',
                'profile' => 'users/profile',
                'string' => 'strings/index',
                'computers' => 'computers/index',
                [
                    'pattern' => 'robots',
                    'route' => 'site/robots',
                    'suffix' => '.txt',
                ],
                'about-us' => 'site/about',
                'why-us' => 'site/why',
                'mission' => 'site/mission',
                'vacancy' => 'site/vacancy',
                'contact' => 'site/contact',
                'production' => 'product/production',
                'grid-gallery' => 'pages/gridgallery',
                'news' => 'news/index',
//                '<action:\w+>' => 'site/<action>',
                '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
            ],
        ],
        'session' => [
            'name' => 'guardsession',
        ],
        'assetManager' => [
            'linkAssets' => true,
        ],
        'i18n' => [
            'translations' => [
                'eauth' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@eauth/messages',
                ],
            ],
        ],
    ],
];

if (!empty($env = $main['params']['env']) && $env === 'dev') {
    // configuration adjustments for 'dev' environment
    $web['bootstrap'][] = 'debug';
    $web['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['*'],
    ];

    $web['bootstrap'][] = 'gii';
    $web['modules']['gii'] = [
        'class' => 'yii\gii\Module',
//        'class' => 'system.gii.GiiModule',
        'allowedIPs' => ['*'],
    ];
//    $web['components']['urlManager']['rules']['gii'] = 'gii';
}

return \yii\helpers\ArrayHelper::merge($main, $web);
