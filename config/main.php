<?php

$local = require(__DIR__ . '/local.php');


$main = [
    'id' => 'gondik',
    'timeZone' => 'Asia/Yekaterinburg',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
    ],
    'language' => 'ru_RU',
    'sourceLanguage' => 'ru',
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.yandex.ru',
                'username' => 'info@gondik.ru',
                'password' => 'zIp9VXdA93',
                'port' => '465',
                'encryption' => 'ssl',
            ],
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'forceTranslation' => true,
                    'sourceLanguage' => 'ru-RU',
                ],
            ],
        ],
        /*'formatter' => [
            'defaultTimeZone' => 'UTC',
        ],*/
    ],
    'params' => [
        'user.passwordResetTokenExpire' => 3600,
        'emailsFrom' => 'info@gondik.ru',
        'emailsSender' => 'Gondik.ru - Студия ландшафтного дизайна Натальи Гондик',
    ],
];
return \yii\helpers\ArrayHelper::merge($main, $local);