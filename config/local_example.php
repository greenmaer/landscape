<?php

$params = [
    'adminEmail' => 'info@gondik.ru', //some email, don't know for what
    'env' => 'prod', //available values dev|prod|test
    'debug' => false, //available values true|false
    'traceLevel' => 3,
    'deployCode' => '123456', //change to uniq string
];

return [
    'components' => [
        'db' => [
            'dsn' => 'mysql:host=localhost;dbname=database_name;charset=UTF8',
            'username' => 'database_user',
            'password' => 'database_user_password',
        ],
        'log' => [
            'traceLevel' => !empty($params['debug']) && $params['debug'] === true
                ? (!empty($params['traceLevel']) ? (integer)$params['traceLevel'] : 3)
                : 0,
        ],
    ],
    'params' => $params,
];