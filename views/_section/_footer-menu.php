<?php
use yii\widgets\Menu;

?>
<div class="cell-sm-12">
	<h5>Useful Links</h5>
	<div class="divider divider-3"></div>
	<div class="range range-30">
		<div class="cell-sm-12">
			<?= Menu::widget([
				'items' => [
					['label' => 'About Us', 'url' => ['site/about']],
					['label' => 'Works', 'url' => ['pages/gridgallery']],
					['label' => 'Production', 'url' => ['product/production']],
					['label' => 'Careers', 'url' => ['site/careers']],
					['label' => 'Products', 'url' => ['/site/product-catalog.html']],
					['label' => 'Blog', 'url' => ['news/index']],
				],
				'encodeLabels' => false,
				'options' => array( 'class' => 'list-marked list-marked-1 list-double')
			]);
			?>
		</div>
	</div>
</div>