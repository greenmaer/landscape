<section class="section bg-image-2 context-dark">
	<div class="shell shell-fluid text-center">
		<div class="range">
			<div class="cell-sm-12 cell-xl-10 cell-xl-preffix-1">
				<div class="range range-condensed decorative-container">
					<div class="cell-sm-6 cell-md-3 decorative-cell">
						<div class="counter-wrap">
							<div class="heading-1"><span class="counter" data-step="100000">13</span></div>
							<div class="decorative-divider"></div>
							<h5 class="counter-description">Years of Experience</h5>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 decorative-cell">
						<div class="counter-wrap">
							<div class="heading-1"><span class="counter" data-step="100000">175</span></div>
							<div class="decorative-divider"></div>
							<h5 class="counter-description">Qualified Specialists</h5>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 decorative-cell">
						<div class="counter-wrap">
							<div class="heading-1"><span class="counter" data-step="100000">840</span></div>
							<div class="decorative-divider"></div>
							<h5 class="counter-description">Landscape Designs</h5>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 decorative-cell">
						<div class="counter-wrap">
							<div class="heading-1"><span class="counter" data-step="100000">18</span></div>
							<div class="decorative-divider"></div>
							<h5 class="counter-description">Partners Worldwide</h5>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>