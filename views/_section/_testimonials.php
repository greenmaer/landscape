<section class="section section-xl bg-image-1 context-dark">
	<div class="relative-block text-center">
		<div class="decorative decorative-lg custom-title">
			<h3 class="decorative-title">Testimonials</h3>
		</div>
		<!-- Owl Carousel-->
		<div class="owl-carousel owl-carousel-numbered owl-carousel-numbered-secondary text-center" data-items="1" data-dots="true" data-nav="false" data-stage-padding="15" data-loop="false" data-margin="30" data-mouse-drag="false" data-animation-in="fadeIn">
			<div class="owl-item">
				<div class="range range-center">
					<div class="cell-sm-9 cell-lg-7 cell-xl-5">
						<blockquote class="quote quote-default quote-secondary">
							<div class="quote-body">
								<div class="icon icon-quote fa-quote-left"></div>
								<p>
									<q>GardenLand is a company that we can rely on.  We often have to react to customer needs quickly and we know that GardenLand will take care of us when we need high-quality landscape design services.</q>
								</p>
							</div>
							<div class="quote-meta">
								<div class="unit unit-spacing-xxs unit-middle unit-vertical text-center">
									<div class="unit__left"><img class="img-circle" src="/images/quote-1-60x60.jpg" alt="" width="60" height="60"/>
									</div>
									<div class="unit__body">
										<cite>Robert Ball</cite><small><span>President of US Landscape Design Corporation,</span><br><span>GardenLand’s partner</span></small>
									</div>
								</div>
							</div>
						</blockquote>
					</div>
				</div>
			</div>
			<div class="owl-item">
				<div class="range range-center">
					<div class="cell-sm-9 cell-lg-7 cell-xl-5">
						<blockquote class="quote quote-default quote-secondary">
							<div class="quote-body">
								<div class="icon icon-quote fa-quote-left"></div>
								<p>
									<q>Wow, I’m so happy with your service. I would just like to say thank you for your prompt and effective landscape design, for your friendly and professional support staff! I will recommend your expert company to all my friends.</q>
								</p>
							</div>
							<div class="quote-meta">
								<div class="unit unit-spacing-xxs unit-middle unit-vertical text-center">
									<div class="unit__left"><img class="img-circle" src="/images/quote-2-60x60.jpg" alt="" width="60" height="60"/>
									</div>
									<div class="unit__body">
										<cite>Joe Sanders</cite><small><span>Web Designer,</span><br><span>GardenLand’s customer</span></small>
									</div>
								</div>
							</div>
						</blockquote>
					</div>
				</div>
			</div>
			<div class="owl-item">
				<div class="range range-center">
					<div class="cell-sm-9 cell-lg-7 cell-xl-5">
						<blockquote class="quote quote-default quote-secondary">
							<div class="quote-body">
								<div class="icon icon-quote fa-quote-left"></div>
								<p>
									<q>I just wanted to let you know that our property has never looked better. What an amazing job your crew does!  You are very professional, hardworking and show great pride in your work.</q>
								</p>
							</div>
							<div class="quote-meta">
								<div class="unit unit-spacing-xxs unit-middle unit-vertical text-center">
									<div class="unit__left"><img class="img-circle" src="/images/quote-4-60x60.jpg" alt="" width="60" height="60"/>
									</div>
									<div class="unit__body">
										<cite>James Johnson</cite><small><span>Businessman,</span><br><span>GardenLand’s regular customer</span></small>
									</div>
								</div>
							</div>
						</blockquote>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>