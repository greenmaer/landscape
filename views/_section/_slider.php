<section class="section">
	<div class="swiper-container swiper-slider swiper-slider-custom" data-autoplay="false" data-simulate-touch="false" data-loop="true">
		<div class="swiper-wrapper text-center">
			<div class="swiper-slide" data-slide-bg="/images/slider-5-1920x700.jpg" data-slide-title="Title text">
				<div class="swiper-slide-caption">
					<div class="shell">
						<div class="range range-center">
							<div class="cell-lg-10">
								<div class="box-content" data-caption-animate="fadeInUp" data-caption-delay="100">
									<h1>Your Lawn and Landscape</h1>
									<h3 class="text-bold">The Way They Should Look</h3>
									<div class="divider divider-4"></div>
									<h5 class="caption">Welcome to the top rated landscape design bureau with a team of real experts in building exclusive modern spaces.</h5><a class="button button-primary" href="/site/product-catalog.html">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide" data-slide-bg="/images/slider-6-1920x700.jpg" data-slide-title="Title text">
				<div class="swiper-slide-caption">
					<div class="shell">
						<div class="range range-center">
							<div class="cell-lg-10">
								<div class="box-content" data-caption-animate="fadeInUp" data-caption-delay="100">
									<h1>Residential and Commercial</h1>
									<h3 class="text-bold">Landscape Designs for Everyone</h3>
									<div class="divider divider-4"></div>
									<h5 class="caption">We provide professional landscape design, maintenance, lighting and irrigation services for residential and commercial properties.</h5><a class="button button-primary" href="/site/product-catalog.html">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="swiper-slide" data-slide-bg="/images/slider-7-1920x700.jpg" data-slide-title="Title text">
				<div class="swiper-slide-caption">
					<div class="shell">
						<div class="range range-center">
							<div class="cell-lg-11">
								<div class="box-content" data-caption-animate="fadeInUp" data-caption-delay="100">
									<h1>Quality Landscape Construction</h1>
									<h3 class="text-bold">By the Award-Winning Team</h3>
									<div class="divider divider-4"></div>
									<h5 class="caption">We have the experience and expertise to manage all aspects of landscape construction and we strive to ensure that our projects will be finished on time and on budget.</h5><a class="button button-primary" href="/site/product-catalog.html">READ MORE</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Swiper pagination-->
		<div class="swiper-pagination"></div>
		<!-- Swiper Navigation-->
		<div class="swiper-button swiper-button-prev"><span class="swiper-button__arrow">
              <svg width="22" height="22" viewbox="0 0 64 64">
				  <path d="M46.077 55.738c0.858 0.867 0.858 2.266 0 3.133s-2.243 0.867-3.101 0l-25.056-25.302c-0.858-0.867-0.858-2.269 0-3.133l25.056-25.306c0.858-0.867 2.243-0.867 3.101 0s0.858 2.266 0 3.133l-22.848 23.738 22.848 23.738z"></path>
			  </svg></span>
			<div class="preview">
				<h3 class="title">Text</h3>
				<div class="preview__img"></div>
			</div>
		</div>
		<div class="swiper-button swiper-button-next"><span class="swiper-button__arrow">
              <svg width="22" height="22" viewbox="0 0 64 64">
				  <path d="M17.919 55.738c-0.858 0.867-0.858 2.266 0 3.133s2.243 0.867 3.101 0l25.056-25.302c0.858-0.867 0.858-2.269 0-3.133l-25.056-25.306c-0.858-0.867-2.243-0.867-3.101 0s-0.858 2.266 0 3.133l22.848 23.738-22.848 23.738z"></path>
			  </svg></span>
			<div class="preview">
				<h3 class="title">Text</h3>
				<div class="preview__img"></div>
			</div>
		</div>
	</div>
</section>