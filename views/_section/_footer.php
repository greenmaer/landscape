<?php
/**
 * @var \yii\web\View $this
 * @var boolean $isHome
 */
?>
<footer class="section page-footer page-footer-dark bg-primary-dark context-dark">
    <div class="shell shell-wide">
        <div class="range range-40">
            <div class="cell-sm-12 cell-lg-6">

                <?= $this->render('/_section/_footer-contacts.php') ?>

            </div>
            <div class="cell-sm-6 cell-lg-3">
                <div class="range range-30 range-xl-60">

                    <?= $this->render('/_section/_footer-menu.php') ?>

                    <?= $this->render('/_section/_footer-tags.php') ?>

                </div>
            </div>
            <div class="cell-sm-6 cell-lg-3 cell-vertial-align">

                <?= $this->render('/_section/_footer-subscribe.php') ?>

                <?= $this->render('/_section/_footer-copyright.php', ['isHome' => $isHome]) ?>

            </div>
        </div>
    </div>
</footer>