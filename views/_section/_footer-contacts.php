
	<h5>Контакты</h5>
	<div class="divider divider-3"></div>
	<div class="range range-40">
		<div class="cell-sm-6">
			<h6>Greenfield Office</h6>
			<div class="decorative decorative-md">
				<ul class="list">
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span></div>
							<div class="unit__body"><a class="link-default" href="#">995 Green Hill St., Greenfield, IN 46140</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-phone"></span></div>
							<div class="unit__body"><a class="link-default" href="callto:#">1-800-1234-567</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-email-outline"></span></div>
							<div class="unit__body"><a class="link-default" href="mailto:#">mail@demolink.org</a></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="cell-sm-6">
			<h6>Whitehall Office</h6>
			<div class="decorative decorative-md">
				<ul class="list">
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span></div>
							<div class="unit__body"><a class="link-default" href="#">273 Johnson Street, Whitehall, PA 18052</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-phone"></span></div>
							<div class="unit__body"><a class="link-default" href="callto:#">1-800-8765-098</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-email-outline"></span></div>
							<div class="unit__body"><a class="link-default" href="mailto:#">mail@demolink.org</a></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="cell-sm-6">
			<h6>New York Office</h6>
			<div class="decorative decorative-md">
				<ul class="list">
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span></div>
							<div class="unit__body"><a class="link-default" href="#">2130 Washington Street, New York, NY 87654</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-phone"></span></div>
							<div class="unit__body"><a class="link-default" href="callto:#">1-800-7658-321</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-email-outline"></span></div>
							<div class="unit__body"><a class="link-default" href="mailto:#">mail@demolink.org</a></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
		<div class="cell-sm-6">
			<h6>Norwalk Office</h6>
			<div class="decorative decorative-md">
				<ul class="list">
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span></div>
							<div class="unit__body"><a class="link-default" href="#">8892 Mayfair St., Norwalk, CT 06851</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-phone"></span></div>
							<div class="unit__body"><a class="link-default" href="callto:#">1-800-8765-098</a></div>
						</div>
					</li>
					<li>
						<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
							<div class="unit__left"><span class="icon icon-xss icon-secondary mdi mdi-email-outline"></span></div>
							<div class="unit__body"><a class="link-default" href="mailto:#">mail@demolink.org</a></div>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>