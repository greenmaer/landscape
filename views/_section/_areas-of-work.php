<?php
use yii\helpers\Url;
?>
<section class="section section-md bg-white">
	<div class="shell shell-fluid">
		<div class="range range-40 range-xl-condensed">
			<div class="cell-md-7 cell-xl-6 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Areas of Work</h3>
					<p>When we take your landscaping project, we never waste your time idly…  Quick on getting all the brief on the ideas you might have, we immediately start the working process, making sure that everything gets done ahead of the schedule!</p>
					<div class="range range-40">
						<div class="cell-sm-6 cell-xl-5">
							<div class="blurb">
								<div class="icon icon-primary icon-xxl fl-line-icon-set-plant10"></div>
								<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Landscape Ideas</a></h5>
								<p class="blurb-content">From finding the best landscape design ideas or inspiration to maintaining your dream yard, the landscaping tips and design ideas will keep you moving. </p>
							</div>
						</div>
						<div class="cell-sm-6 cell-xl-5 cell-xl-preffix-1">
							<div class="blurb">
								<div class="icon icon-primary icon-xxl fl-line-icon-set-diamond18"></div>
								<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Architectural Design Coordination</a></h5>
								<p class="blurb-content">Architectural design is typically a multi-staged process, involving a number of designers working together to create a single project that can improve your yard.</p>
							</div>
						</div>
						<div class="cell-sm-6 cell-xl-5">
							<div class="blurb">
								<div class="icon icon-primary icon-xxl fl-line-icon-set-cogwheel8"></div>
								<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Landscape Maintenance</a></h5>
								<p class="blurb-content">We will customize a regular service plan just for your yard, focused on highlighting its natural beauty. We will maintain your landscape so you can enjoy satisfaction.</p>
							</div>
						</div>
						<div class="cell-sm-6 cell-xl-5 cell-xl-preffix-1">
							<div class="blurb">
								<div class="icon icon-primary icon-xxl fl-line-icon-set-square51"></div>
								<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Construction Consultation</a></h5>
								<p class="blurb-content">GardenLand provides comprehensive construction management consulting services that provide the best results for your project, whether it’s commercial or individual.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="cell-md-5 cell-xl-3 cell-xl-preffix-1">
				<div class="thumbnail-type-3"><img src="/images/index-1-480x400.jpg" alt="" width="480" height="400"/>
					<div class="caption">
						<h5 class="text-bold title">About Us</h5>
						<p class="description">Over the last 13 years, GardenLand has created numerous award-winning luxury outdoor living areas. We can complete landscape design projects of various complexity.</p><a class="post-link" href="<?= Url::toRoute(['site/about-us']) ?>">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>