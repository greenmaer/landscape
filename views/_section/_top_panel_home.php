<div class="rd-navbar-outer outer-1 rd-navbar-top-panel">
	<div class="rd-navbar-inner">
		<div class="rd-navbar-cell rd-navbar-brand-default">
			<!-- RD Navbar Brand--><a class="rd-navbar-brand" href="./"><img class="brand-normal" src="/images/brand.png" width="180" height="33" alt=""></a>
		</div>
		<div class="rd-navbar-cell">
			<ul class="navbar-inline-list">
				<li class="rd-navbar-info">
					<div class="icon icon-xs icon-primary mdi mdi-email-outline"></div><a class="link" href="mailto:info@gondik.ru">info@gondik.ru</a>
				</li>
				<li class="rd-navbar-info">
					<div class="icon icon-xs icon-primary mdi mdi-phone"></div><a class="link" href="callto:+79091163758">+7 (909) 116-37-58</a>
				</li>
			</ul>
		</div>
	</div>
</div>