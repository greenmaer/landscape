<?php
/**
 * @var \yii\web\View $this
 * @var boolean $isHome
 */
?>
<div class="block-bottom text-sm-right"><a class="brand-name" href="/">
        <img src="/images/brand.png" width="146" height="24" alt=""></a>
    <p class="small">&#169; <span id="copyright-year"></span> Все права защищены
        <!--        <a class="link-default" href="/site/terms-of-use.html">Terms of Use</a> and <a class="link-default" href="/site/privacy-policy.html">Privacy Policy</a>-->
        <?php //if ($isHome): ?>
        <!--    More <a rel="nofollow"-->
        <!--            href="http://www.templatemonster.com/category.php?category=214&type=1"-->
        <!--            target="_blank">Garden Design Templates at TemplateMonster.com</a>-->
        <?php //endif; ?>
    </p>
</div>