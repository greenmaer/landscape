<div class="block-top">
	<h5>Newsletter</h5>
	<div class="divider divider-3"></div>
	<p>Subscribe to our newsletter to receive updates on our company's products, news, and special offers.</p>
	<!-- RD Mailform-->
	<form class="rd-mailform rd-mailform-inline rd-mailform-small text-left" data-form-output="form-output-global" data-form-type="forms" method="post" action="bat/rd-mailform.php">
		<div class="form-wrap">
			<label class="form-label" for="inline-email-1">Enter your e-mail</label>
			<input class="form-input" id="inline-email-1" type="email" name="email" data-constraints="@Email @Required">
		</div>
		<button class="button button-sm button-primary" type="submit">Subscribe</button>
	</form>
</div>