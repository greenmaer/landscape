<!-- RD Navbar Panel Right-->
<div class="rd-navbar-cell rd-navbar-panel-right">
	<!-- Search toggle-->
	<button class="search-toggle" data-rd-navbar-toggle=".rd-navbar-search"><span class="toggle-icon"></span></button>
	<!-- Search-->
	<div class="rd-navbar-search">
		<form class="rd-search" action="/site/search-results.html" data-search-live="rd-search-results-live" method="GET">
			<div class="form-wrap">
				<label class="form-label form-label" for="rd-navbar-search-form-input">Type and hit enter...</label>
				<input class="rd-navbar-search-form-input form-input" id="rd-navbar-search-form-input" type="text" name="s" autocomplete="off">
				<button class="button-submit fa fa-search" type="submit"></button>
				<div class="rd-search-results-live" id="rd-search-results-live"></div>
			</div>
		</form>
	</div>
</div>