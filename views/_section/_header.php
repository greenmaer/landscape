<header class="section page-header">
    <!-- RD Navbar-->
    <div class="rd-navbar-wrap">
        <?php if ($isHome): ?>
            <nav class="rd-navbar rd-navbar-default rd-navbar-default-top-panel" data-sm-stick-up-offset="1px"
                 data-md-stick-up-offset="44px" data-lg-stick-up-offset="70px" data-layout="rd-navbar-fixed"
                 data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fullwidth"
                 data-md-device-layout="rd-navbar-fixed" data-lg-device-layout="rd-navbar-fixed"
                 data-lg-layout="rd-navbar-static" data-sm-stick-up="true" data-md-stick-up="true"
                 data-lg-stick-up="true">
                <!-- RD Navbar Top Panel-->
                <button class="collapse-toggle" data-rd-navbar-toggle=".rd-navbar-top-panel"><span
                            class="toggle-icon"></span></button>

                <?= $this->render('../_section/_top_panel_home.php') ?>

                <div class="rd-navbar-outer outer-2">
                    <div class="rd-navbar-inner">
                        <!-- RD Navbar Panel-->
                        <div class="rd-navbar-cell rd-navbar-panel">
                            <!-- RD Navbar Toggle-->
                            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav"><span
                                        class="toggle-icon"></span></button>
                            <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="./"><img class="brand-mini"
                                                                                             src="/images/brand-mini.png"
                                                                                             width="180" height="33"
                                                                                             alt=""></a>
                        </div>

                        <?= $this->render('../_section/_menu.php') ?>

                        <?php
                        //echo $this->render('../_section/_search.php')
                        ?>

                    </div>
                </div>
            </nav>
        <?php else : ?>
            <nav class="rd-navbar rd-navbar-transparent" data-sm-stick-up-offset="1px" data-md-stick-up-offset="92px"
                 data-lg-stick-up-offset="54px" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed"
                 data-md-layout="rd-navbar-fullwidth" data-md-device-layout="rd-navbar-fixed"
                 data-lg-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-sm-stick-up="true"
                 data-md-stick-up="true" data-lg-stick-up="true">
                <!-- RD Navbar Top Panel-->
                <button class="collapse-toggle" data-rd-navbar-toggle=".rd-navbar-top-panel"><span
                            class="toggle-icon"></span></button>

                <?= $this->render('../_section/_top_panel_other.php') ?>

                <div class="rd-navbar-outer outer-transparent">
                    <div class="rd-navbar-inner">
                        <!-- RD Navbar Panel-->
                        <div class="rd-navbar-cell rd-navbar-panel">
                            <!-- RD Navbar Toggle-->
                            <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav"><span
                                        class="toggle-icon"></span></button>
                            <!-- RD Navbar Brand--><a class="rd-navbar-brand" href="./"><img class="brand-normal"
                                                                                             src="/images/brand.png"
                                                                                             width="169" height="23"
                                                                                             alt=""><img
                                        class="brand-mini" src="/images/brand-mini.png" width="169" height="23" alt=""></a>
                        </div>

                        <?= $this->render('../_section/_menu.php') ?>

                        <?php
                        //echo $this->render('../_section/_search.php')
                        ?>

                    </div>
                </div>
            </nav>
        <?php endif; ?>
    </div>
</header>