<?php
/**
 * @var \yii\web\View $this
 */

use yii\helpers\Html;

?>
<div class="rd-navbar-outer outer-1 rd-navbar-top-panel">
    <div class="rd-navbar-inner">
        <div class="rd-navbar-cell">
            <ul class="navbar-inline-list">
                <li class="rd-navbar-info">
                    <div class="icon icon-xs mdi mdi-map-marker"></div>
                    <?= Html::a('г.Пермь, ул.Свиязева 43', ['site/contact'],
                        ['class' => 'link']) ?>
                </li>
                <li class="rd-navbar-info">
                    <div class="icon icon-xs icon-primary mdi mdi-email-outline"></div>
                    <a class="link" href="mailto:info@gondik.ru">info@gondik.ru</a>
                </li>
                <li class="rd-navbar-info">
                    <div class="icon icon-xs icon-primary mdi mdi-phone"></div>
                    <a class="link" href="callto:+79091173758">+7 (909) 1163 758</a>
                </li>
                <li class="rd-navbar-info">
                    <div class="icon icon-xs mdi mdi-login"></div>
                    <a class="link login" href="/site/login-register.html">Login/Register</a>
                </li>
            </ul>
        </div>
        <div class="rd-navbar-cell">
            <div class="rd-navbar-social"><a class="link fa fa-facebook" href="#"></a><a class="link fa fa-twitter"
                                                                                         href="#"></a><a
                        class="link fa fa-pinterest" href="#"></a><a class="link fa fa-vimeo" href="#"></a><a
                        class="link fa fa-google" href="#"></a><a class="link fa fa-rss" href="#"></a></div>
        </div>
    </div>
</div>