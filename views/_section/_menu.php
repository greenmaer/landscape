<?php
/**
 * @var \yii\web\View $this
 */
use yii\bootstrap\Nav;
//use app\extended\yii\bootstrap\Nav;
use yii\widgets\Menu;

switch ($this->context->route) {
    case 'site/about':
        $active = 'about';
        break;
    case 'site/mission':
        $active = 'about';
        break;
    case 'site/why-us':
        $active = 'about';
        break;
    case 'pages/gridgallery':
        $active = 'pages';
        break;
    case 'news/index':
        $active = 'news';
        break;

    default:
        $active = null;
        break;
}
$id = 5

?>

<!-- RD Navbar Nav-->
<div class="rd-navbar-cell rd-navbar-nav-wrap">
    <!-- RD Navbar Nav-->
    <?php
    echo Nav::widget([
        'options' => [
            'class' => 'rd-navbar-nav',
        ],
        'dropDownCaret' => '',
        'dropdownClass' => 'app\extended\yii\bootstrap\Dropdown',
        'items' => [
            [
                'label' => 'Главная',
                'url' => ['/site/index'],
            ],
            [
                'label' => 'О нас',
                'options' => [
                    'class' => 'rd-navbar-dropdown',
                ],
                'dropDownOptions' => [
                    'class' => 'rd-navbar-dropdown rd-navbar-open-right',
                ],
                'items' => [
                    [
                        'label' => 'О компании',
                        'url' => ['site/about'],
                    ],
                    [
                        'label' => 'Миссия',
                        'url' => ['site/mission'],
                    ],
                    [
                        'label' => 'Почему мы',
                        'url' => ['site/why'],
                    ],
                ],
            ],
			[
				'label' => 'Услуги',
				'url' => ['product/production']
			],
			[
				'label' => 'Products',
				'options' => [
					'class' => 'rd-navbar-dropdown',
				],
				'dropDownOptions' => [
					'class' => 'rd-navbar-dropdown rd-navbar-open-right',
				],

				'items' => [
					[
						'label' => 'Product Catalog',
						'url' => ['/site/product-catalog.html'],
					],
					[
						'label' => 'Single product',
						'url' => ['product/' . $id],
					],
				],
			],
			[
				'label' => 'Вакансии',
				'url' => ['site/vacancy']
			],
			[
				'label' => 'Pages',
				'options' => [
					'class' => 'rd-navbar-dropdown',
				],
				'dropDownOptions' => [
					'class' => 'rd-navbar-dropdown rd-navbar-open-right',
				],
				'items' => [
					[
						'label' => 'Pages',
						'options' => [
							'class' => 'rd-megamenu-list',
						],
						'items' => [
							[
								'label' => '404 Page',
								'url' => ['/site/404-page.html']
							],
							[
								'label' => '503 Page',
								'url' => ['/site/503-page.html']
							],
							[
								'label' => 'Under Construction',
								'url' => ['/site/under-construction.html']
							],
							[
								'label' => 'Coming Soon',
								'url' => ['/site/coming-soon.html']
							],
							[
								'label' => 'Search Results',
								'url' => ['/site/search-results.html']
							],
							[
								'label' => 'Terms of Use',
								'url' => ['/site/terms-of-use.html']
							],
							[
								'label' => 'Login/Register',
								'url' => ['/site/login-register.html']
							],
						],
					],
					[
						'label' => 'Gallery',
						'options' => [
							'class' => 'rd-megamenu-list',
						],
						'items' => [
							[
								'label' => 'Grid Gallery',
								'url' => ['pages/gridgallery']
							],
							[
								'label' => 'Masonry Gallery',
								'url' => ['/site/masonry-gallery.html']
							],
							[
								'label' => 'Cobbles Gallery',
								'url' => ['/site/cobbles-gallery.html']
							],
						],
					],
					[
						'label' => 'Elements',
						'options' => [
							'class' => 'rd-megamenu-list',
						],
						'items' => [
							[
								'label' => 'Typography',
								'url' => ['/site/typography.html']
							],
							[
								'label' => 'Buttons',
								'url' => ['/site/buttons.html']
							],
							[
								'label' => 'Forms',
								'url' => ['/site/forms.html']
							],
							[
								'label' => 'Tabs &amp; Accordions',
								'url' => ['/site/tabs-and-accordions.html']
							],
							[
								'label' => 'Progress Bars',
								'url' => ['/site/progress-bars.html']
							],
							[
								'label' => 'Tables',
								'url' => ['/site/tables.html']
							],
							[
								'label' => 'Icons',
								'url' => ['/site/icons.html']
							],
							[
								'label' => 'Grid',
								'url' => ['/site/grid.html']
							],
						],
					],
				],
			],
			[
				'label' => 'Новости',
				'options' => [
					'class' => 'rd-navbar-dropdown',
				],
				'dropDownOptions' => [
					'class' => 'rd-navbar-dropdown rd-navbar-open-right',
				],
				'items' => [
					[
						'label' => 'Classic News',
						'url' => ['news/index']
					],
					[
						'label' => 'Grid News',
						'url' => ['/site/grid-news.html']
					],
					[
						'label' => 'Masonry News',
						'url' => ['/site/masonry-news.html']
					],
					[
						'label' => 'Modern News',
						'url' => ['/site/modern-news.html']
					],
					[
						'label' => 'Single Post',
						'url' => ['news/' . $id]
					],
				],
			],
			[
				'label' => 'Контакты',
				'url' => ['site/contact']
			],
        ],
        //'submenuTemplate'=>
    ]);
    ?>
</div>
