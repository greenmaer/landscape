<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

if (!empty($type)) {
    switch ($type) {
        case 'news':
            $this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['news/index']];
            break;
        case 'product':
            $this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/site/product-catalog.html']];
            break;
    }
}

$this->params['breadcrumbs'][] = $this->title;
?>

<section class="section section-bredcrumbs bg-image-breadcrumbs-1">
	<div class="shell-fluid context-dark">
		<div class="range range-condensed">
			<div class="cell-xs-10 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h1 class="decorative-title"><?= $this->title ?></h1>
				</div>
				<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
					'options' => ['class'=>'breadcrumbs-custom'],
					'tag'   =>  'ol',
				]); ?>
			</div>
		</div>
	</div>
</section>