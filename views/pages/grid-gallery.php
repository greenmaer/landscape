
<?= $this->render('../_section/_breadcrumbs.php') ?>

<!--Search results-->
<section class="section section-md">
	<div class="shell shell-fluid">
		<div class="range range-condensed">
			<div class="cell-sm-12 cell-xl-10 cell-xl-preffix-1 cell-xl-postfix-1">
				<div class="row row-no-gutter" data-photo-swipe-gallery="gallery">
					<!-- Isotope Filters-->
					<div class="col-lg-12">
						<div class="isotope-filters isotope-filters-horizontal">
							<button class="isotope-filters-toggle button button-sm button-primary" data-custom-toggle="#isotope-filters" data-custom-toggle-disable-on-blur="true">Filter<span class="caret"></span></button>
							<ul class="isotope-filters-list" id="isotope-filters">
								<li><a class="active" data-isotope-filter="*" data-isotope-group="gallery" href="#">All Types</a></li>
								<li><a data-isotope-filter="Category 1" data-isotope-group="gallery" href="#">Type 1</a></li>
								<li><a data-isotope-filter="Category 2" data-isotope-group="gallery" href="#">Type 2</a></li>
								<li><a data-isotope-filter="Category 3" data-isotope-group="gallery" href="#">Type 3</a></li>
							</ul>
						</div>
					</div>
					<!-- Isotope Content-->
					<div class="col-lg-12">
						<div class="isotope" data-isotope-layout="fitRows" data-isotope-group="gallery">
							<div class="row row-no-gutter">
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="*"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1199x800" href="/images/gallery-image-original-1-1199x800.jpg"><img src="/images/grid-gallery-1-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Functional Design Layout</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 3"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1119x800" href="/images/gallery-image-original-2-1119x800.jpg"><img src="/images/grid-gallery-2-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Professional Lawn Design</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 3"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x732" href="/images/gallery-image-original-3-1200x732.jpg"><img src="/images/grid-gallery-3-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Landscape Architecture</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 3"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x797" href="/images/gallery-image-original-4-1200x797.jpg"><img src="/images/grid-gallery-4-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Pond Construction</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 3"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-5-1200x800.jpg"><img src="/images/grid-gallery-5-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Exterior Design</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 2"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1195x800" href="/images/gallery-image-original-6-1195x800.jpg"><img src="/images/grid-gallery-6-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Yard Remodeling</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 2"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-7-1200x800.jpg"><img src="/images/grid-gallery-7-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Principles of Landscape Design</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 2"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-8-1200x800.jpg"><img src="/images/grid-gallery-8-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Design for Backyards</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 2"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-9-1200x800.jpg"><img src="/images/grid-gallery-9-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Garden Styles</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 1"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1054x800" href="/images/gallery-image-original-10-1054x800.jpg"><img src="/images/grid-gallery-10-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Planting Ideas</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 1"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1089x800" href="/images/gallery-image-original-11-1089x800.jpg"><img src="/images/grid-gallery-11-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Custom Landscape Design Projects</h6>
										</div></a>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-3 isotope-item" data-filter="Category 1"><a class="thumbnail-type-2" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-12-1200x800.jpg"><img src="/images/grid-gallery-12-400x270.jpg" alt="" width="400" height="270"/>
										<div class="caption">
											<div class="icon icon-white mdi mdi-magnify-plus"></div>
											<h6 class="title">Lawn Maintenance</h6>
										</div></a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>