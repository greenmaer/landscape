<?php
/**
 * @var \yii\web\View $this
 * @var string $content
 */

use app\assets\AppAsset;
use yii\helpers\Html;

use app\widgets\Alert;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\widgets\Menu;

AppAsset::register($this);

$isHome = (!empty($this->params['homepage']) && $this->params['homepage'])?true:false;
?>
<!DOCTYPE html>
<?php $this->beginPage() ?>
    <html class="wide smoothscroll wow-animation" lang="<?= Yii::$app->language ?>">
        <head>
            <title><?= Html::encode($this->title) ?></title>
            <!--<title>Home</title>-->
            <meta name="format-detection" content="telephone=no">
            <meta name="viewport"
                  content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta charset="<?= Yii::$app->charset ?>">
            <?php $this->registerCsrfMetaTags() ?>
            <link rel="icon" href="/images/favicon.ico" type="image/x-icon">
            <?php $this->head() ?>
            <!--[if lt IE 10]>
            <div style="background: #212121; padding: 10px 0; box-shadow: 3px 3px 5px 0 rgba(0,0,0,.3); clear: both; text-align:center; position: relative; z-index:1;"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="/images/ie8-panel/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
            <script src="/js/html5shiv.min.js"></script>
            <![endif]-->
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript" >
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function() {
                        try {
                            w.yaCounter48053768 = new Ya.Metrika2({
                                id:48053768,
                                clickmap:true,
                                trackLinks:true,
                                accurateTrackBounce:true,
                                webvisor:true
                            });
                        } catch(e) { }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () { n.parentNode.insertBefore(s, n); };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/tag.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else { f(); }
                })(document, window, "yandex_metrika_callbacks2");
            </script>
            <noscript><div><img src="https://mc.yandex.ru/watch/48053768" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
            <!-- /Yandex.Metrika counter -->
        </head>
        <body>
            <?php $this->beginBody() ?>
                <div class="page">
                    <!-- Page preloader-->
                    <?= $this->render('../_section/_page-loader.php') ?>

                    <!-- Header-->
                    <?= $this->render('../_section/_header.php',['isHome'=>$isHome]) ?>

                    <?= $content ?>

                    <!-- Page Footer-->
                    <?= $this->render('../_section/_footer.php',['isHome'=>$isHome]) ?>
                </div>
                <!-- Global Mailform Output-->
            <?= $this->render('../_section/_snackbars.php') ?>

                <!-- PhotoSwipe Gallery-->
            <?= $this->render('../_section/_photo-swipe-gallery.php') ?>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>