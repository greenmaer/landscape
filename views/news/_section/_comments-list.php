
<section class="section-lg bg-white">
	<div class="decorative decorative-lg">
		<h3 class="decorative-title">Comments</h3>
		<div class="box-comment">
			<div class="unit unit-xs-horizontal unit-spacing-md">
				<div class="unit__left"><img src="/images/box-comment-1-90x90.jpg" alt="" width="90" height="90"/>
				</div>
				<div class="unit__body">
					<ul class="list-inline list-inline-md">
						<li><a href="#">Mildred McCoy</a></li>
						<li class="comment-meta"><span>November 9, 2017,</span> at <span>1:34pm</span>
						</li>
					</ul>
					<p class="comment">I think the award you won is clearly the best proof how good your services are. Our yard always looks perfect after your professional touch. I hope to read more such articles about your company in the future.</p>
				</div>
			</div>
		</div>
		<div class="box-comment box-comment-reply">
			<div class="unit unit-xs-horizontal unit-spacing-md">
				<div class="unit__left"><img src="/images/box-comment-2-90x90.jpg" alt="" width="90" height="90"/>
				</div>
				<div class="unit__body">
					<ul class="list-inline list-inline-md">
						<li><a href="#">Kevin Wade</a></li>
						<li class="comment-meta"><span>November 9, 2017,</span> at <span>2:56pm</span>
						</li>
						<li class="author-reply">
							<div class="icon icon-xxs icon-gray fa-reply"></div><span>Mildred McCoy</span>
						</li>
					</ul>
					<p class="comment">Thank you for your comment! I also hope that we will achieve even more with our new projects that our designers are now working on.</p>
				</div>
			</div>
		</div>
	</div>
</section>