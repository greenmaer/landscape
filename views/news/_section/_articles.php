<?php
use yii\helpers\Url;
?>
<section>
	<!-- Post Classic-->
	<article class="post post-classic">
		<!-- Post media--><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img class="img-responsive" src="/images/post-classic-1-960x500.jpg" alt="" width="960" height="500"/></a>
		<!-- Post content-->
		<section class="post-content text-left">
			<div class="decorative decorative-lg">
				<h3 class="decorative-title"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Winners of Best Landscape for Summer  2017: Latest Results and Comments</a></h3>
				<ul class="post-meta list-dotted">
					<li>
						<time datetime="2017-01-01">January 22, 2017</time>
					</li>
					<li><span class="text-base">by:</span> <a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
					</li>
					<li><a class="link" href="#">News</a>
					</li>
					<li><a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">12 <span class="text-base">Comments</span></a></li>
				</ul>
				<p class="post-description">Annual survey and analysis of community of over 35 million monthly users reveals the top-rated home remodeling professionals and most popular landscape designs. Houzz Inc., the leading platform for home renovation and design, today announced the community’s picks for Best Landscape 2016 in North America, a homeowner-to-homeowner guide to the top...</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read More</a>
			</div>
		</section>
	</article>
	<!-- Post Classic-->
	<article class="post post-classic">
		<!-- Post media--><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img class="img-responsive" src="/images/post-classic-2-960x500.jpg" alt="" width="960" height="500"/></a>
		<!-- Post content-->
		<section class="post-content text-left">
			<div class="decorative decorative-lg">
				<h3 class="decorative-title"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Triple Victory at Inaugural WA Landscape  Design Awards</a></h3>
				<ul class="post-meta list-dotted">
					<li>
						<time datetime="2017-01-01">January 20, 2017</time>
					</li>
					<li><span class="text-base">by:</span> <a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
					</li>
					<li><a class="link" href="#">News</a>
					</li>
					<li><a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">12 <span class="text-base">Comments</span></a></li>
				</ul>
				<p class="post-description">The awards aimed to recognise and showcase the very best landscape design projects in WA. We were thrilled to achieve an award for all three categories we entered into.  We had some very demanding, yet professional judges who are also well-respected landscape designers and media identities. They have also picked other favorites that we would like to notice as well. These...</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read More</a>
			</div>
		</section>
	</article>
	<!-- Post Classic-->
	<article class="post post-classic">
		<!-- Post media--><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img class="img-responsive" src="/images/post-classic-3-960x500.jpg" alt="" width="960" height="500"/></a>
		<!-- Post content-->
		<section class="post-content text-left">
			<div class="decorative decorative-lg">
				<h3 class="decorative-title"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">The Right Stone for your Garden Design: Our List of the Top 3 Recommendations</a></h3>
				<ul class="post-meta list-dotted">
					<li>
						<time datetime="2017-01-01">January 18, 2017</time>
					</li>
					<li><span class="text-base">by:</span> <a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
					</li>
					<li><a class="link" href="#">News</a>
					</li>
					<li><a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">12 <span class="text-base">Comments</span></a></li>
				</ul>
				<p class="post-description">Stone has always been a cost-effective hardscape material for all garden styles. Nowadays they are used as an affordable and easy-to-install surface in contemporary gardens. Their larger relations, the pebbles, cobbles and paddle stones, have become a much more interesting and useful material in our gardens, as they have so many applications.</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read More</a>
			</div>
		</section>
	</article>
	<!-- Post Classic-->
	<article class="post post-classic">
		<!-- Post media--><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img class="img-responsive" src="/images/post-classic-4-960x500.jpg" alt="" width="960" height="500"/></a>
		<!-- Post content-->
		<section class="post-content text-left">
			<div class="decorative decorative-lg">
				<h3 class="decorative-title"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Why Exterior Design is Important</a></h3>
				<ul class="post-meta list-dotted">
					<li>
						<time datetime="2017-01-01">January 22, 2017</time>
					</li>
					<li><span class="text-base">by:</span> <a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
					</li>
					<li><a class="link" href="#">News</a>
					</li>
					<li><a class="link link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">12 <span class="text-base">Comments</span></a></li>
				</ul>
				<p class="post-description">It is vital to have the house look attractive from the outside, as it is the face of you and your family. In some way it gives a picture of the people who live in it, their likes, hobbies, etc. So a good exterior is as important as a proper interior. There are hundreds designs and styles used in landscape.</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read More</a>
			</div>
		</section>
	</article><a class="button button-primary button-icon button-icon-left" href="#"><span class="icon fa-refresh"></span>Load more posts</a>
</section>