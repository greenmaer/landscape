<?php
use yii\helpers\Url;
?>
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-30 range-center range-md-justify range-xl-condensed">
			<div class="cell-sm-10 cell-md-5 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Latest Blog Posts</h3>
				</div>
			</div>
			<div class="cell-sm-10 cell-md-5 text-md-right cell-xl-postfix-1"><a class="button button-primary" href="<?= Url::to(['news/index']) ?>">View all news</a></div>
		</div>

		<?= $this->render('_same-latest.php') ?>

	</div>
</section>