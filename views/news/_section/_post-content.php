<?php
use yii\helpers\Url;
?>
<section class="post-single-body section-lg bg-white">
	<div class="decorative decorative-lg">
		<h3 class="decorative-title post-heading">Winners of Best Landscape for Summer  2017: Latest Results and Comments</h3>
		<ul class="post-meta list-dotted">
			<li><span class="time">July 22, 2017</span></li>
			<li><span class="author">by Kevin Wade</span></li>
			<li><a class="post-tag" href="<?= Url::toRoute(['news/index']) ?>">News</a></li>
			<li><span class="post-comments">2 Comments</span></li>
		</ul>
	</div><img src="/images/news-single-1-960x500.jpg" alt="" width="960" height="500"/>
	<div class="decorative decorative-lg">
		<p>Annual survey and analysis of community of over 35 million monthly users reveals the top-rated home remodeling professionals and most popular landscape designs.</p>
		<p>Houzz Inc., the leading platform for home renovation and design, today announced the community’s picks for Best Landscape 2016 in North America, a homeowner-to-homeowner guide to the top  interior designers, landscape pros and other residential remodeling professionals on Houzz from cabinetry or roofing pros to painters.</p>
	</div>
	<blockquote class="quote quote-default quote-primary">
		<div class="quote-body">
			<p>
				<q>The results of this year’s “Best Landscape” event were somehow predictable yet amazing. We are glad that our partner, GardenLand, took the first place in this rating. It shows the quality of the services they provide.</q>
			</p>
		</div>
		<div class="quote-meta">
			<div class="unit unit-spacing-xxs unit-middle unit-horizontal text-left">
				<div class="unit__left"><img class="img-circle" src="/images/quote-1-60x60.jpg" alt="" width="60" height="60"/>
				</div>
				<div class="unit__body">
					<cite>Robert Ball</cite><small><span>President of US Landscape Design Corporation,</span><br><span>GardenLand’s partner</span></small>
				</div>
			</div>
		</div>
	</blockquote><img src="/images/news-single-2-960x500.jpg" alt="" width="960" height="500"/>
	<div class="decorative decorative-lg">
		<p>The Best Landscapes Award is presented annually. Award winners’ work was the most popular among the more than 35 million monthly users on Houzz.</p>
		<p>Customer Service honors are based on several factors, including the number and quality of recent client reviews. Architecture and interior design photographers whose images were most popular are recognized with the Photography award. A “Best Of Houzz 2016” badge appears on winners’ profiles, as a sign of their commitment to excellence.</p>
		<div class="range social-section">
			<div class="cell-xl-6">
				<ul class="list-tags">
					<li><a href="#">Design</a></li>
					<li><a href="#">Exterior</a></li>
					<li><a href="#">Event</a></li>
					<li><a href="#">News</a></li>
				</ul>
			</div>
			<div class="cell-xl-6">
				<ul class="list-inline list-inline-sm text-xl-right">
					<li><span>Share This Post!</span></li>
					<li><a class="icon icon-xxs fa fa-facebook" href="#"></a></li>
					<li><a class="icon icon-xxs fa fa-twitter" href="#"></a></li>
					<li><a class="icon icon-xxs fa fa-pinterest" href="#"></a></li>
					<li><a class="icon icon-xxs fa fa-vimeo" href="#"></a></li>
					<li><a class="icon icon-xxs fa fa-google" href="#"></a></li>
					<li><a class="icon icon-xxs fa fa-rss" href="#"></a></li>
				</ul>
			</div>
		</div>
	</div>
</section>