<?php
use yii\helpers\Url;
?>

<div class="range range-50 range-center range-md-justify range-xl-condensed">
	<div class="cell-sm-9 cell-md-4 cell-xl-3 cell-xl-preffix-1">
		<article class="post-default">
			<div class="post-media"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/mission-post-1-480x360.jpg" alt="" width="480" height="360"/></a></div>
			<div class="post-title">
				<div class="decorative decorative-md">
					<h5 class="decorative-title"><a class="text-gray-2" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Winners of Best Landscape for Summer  2017: Latest Results and Comments</a></h5>
					<ul class="post-meta list-dotted">
						<li><span class="time">July 22, 2017</span></li>
						<li class="author">by <a class="link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
						</li>
					</ul>
					<div class="post-content">
						<p>Annual survey and analysis of community of over 35 million monthly users reveals the top-rated home remodeling professionals and most popular landscape designs.</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read more</a>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="cell-sm-9 cell-md-4 cell-xl-3">
		<article class="post-default">
			<div class="post-media"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/mission-post-2-480x360.jpg" alt="" width="480" height="360"/></a></div>
			<div class="post-title">
				<div class="decorative decorative-md">
					<h5 class="decorative-title"><a class="text-gray-2" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Triple Victory at Inaugural WA Landscape  Design Awards</a></h5>
					<ul class="post-meta list-dotted">
						<li><span class="time">July 22, 2017</span></li>
						<li class="author">by <a class="link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
						</li>
					</ul>
					<div class="post-content">
						<p>The awards aimed to recognise and showcase the very best landscape design projects in WA. We were thrilled to achieve an award for all three categories we entered into.</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read more</a>
					</div>
				</div>
			</div>
		</article>
	</div>
	<div class="cell-sm-9 cell-md-4 cell-xl-3 cell-xl-postfix-1">
		<article class="post-default">
			<div class="post-media"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/mission-post-3-480x360.jpg" alt="" width="480" height="360"/></a></div>
			<div class="post-title">
				<div class="decorative decorative-md">
					<h5 class="decorative-title"><a class="text-gray-2" href="<?= Url::to(['news/view', 'id' => 5]) ?>">The Right Stone for your Garden Design: Our List of the Top 3 Recommendations</a></h5>
					<ul class="post-meta list-dotted">
						<li><span class="time">July 22, 2017</span></li>
						<li class="author">by <a class="link-default" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Kevin Wade</a>
						</li>
					</ul>
					<div class="post-content">
						<p>Stone has always been a cost-effective hardscape material for all garden styles. Nowadays they are used as an affordable and easy-to-install surface in contemporary gardens.</p><a class="post-link" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Read more</a>
					</div>
				</div>
			</div>
		</article>
	</div>
</div>