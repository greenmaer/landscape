
<div class="cell-sm-6 cell-md-12">
	<div class="decorative">
		<div class="decorative decorative-md">
			<h5 class="decorative-title">Gallery</h5>
			<ul class="gallery-custom" data-photo-swipe-gallery="gallery">
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-13-1200x800.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-1-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="1200x778" href="/images/gallery-image-original-14-1200x778.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-2-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="1200x797" href="/images/gallery-image-original-15-1200x797.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-3-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="923x800" href="/images/gallery-image-original-16-923x800.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-4-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-17-1200x800.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-5-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
				<li><a class="thumbnail-classic" data-photo-swipe-item="" data-size="1200x800" href="/images/gallery-image-original-7-1200x800.jpg">
						<div class="thumbnail-overlay"><img src="/images/aside-gallery-6-140x140.jpg" alt="" width="140" height="140"/>
						</div></a>
				</li>
			</ul>
		</div>
	</div>
</div>