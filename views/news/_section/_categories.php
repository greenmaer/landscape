<div class="cell-sm-6 cell-md-12">
	<div class="decorative">
		<div class="decorative decorative-md">
			<h5 class="decorative-title">Categories</h5>
			<ul class="list-marked list-marked-1">
				<li><a href="#">News</a></li>
				<li><a href="#">Design</a></li>
				<li><a href="#">Garden Sculptures</a></li>
				<li><a href="#">Landscape Design</a></li>
				<li><a href="#">Review</a></li>
				<li><a href="#">Exterior</a></li>
			</ul>
		</div>
	</div>
</div>