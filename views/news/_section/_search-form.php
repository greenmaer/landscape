<div class="cell-sm-12 cell-md-12">
	<div class="decorative decorative-md">
		<h5 class="decorative-title">Search</h5>
		<!-- Search Form-->
		<form class="form-search rd-search rd-mailform-inline rd-mailform-small" action="/site/search-results.html" method="GET">
			<div class="form-wrap">
				<label class="form-label form-search-label form-label-sm" for="blog-sidebar-2-form-search-widget">Search</label>
				<input class="form-input form-search-input form-control #{inputClass}" id="blog-sidebar-2-form-search-widget" type="text" name="s" autocomplete="off">
			</div>
			<button class="button button-sm button-primary form-search-submit" type="submit">Search</button>
		</form>
	</div>
</div>