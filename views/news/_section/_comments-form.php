<section class="section-lg bg-white">
	<div class="decorative decorative-lg">
		<h3 class="decorative-title">Send a Comment</h3>
		<!-- RD Mailform-->
		<form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="forms" method="post" action="bat/rd-mailform.php">
			<div class="range range-xs-center range-15">
				<div class="cell-sm-6">
					<div class="form-wrap form-wrap-validation">
						<label class="form-label form-label-outside" for="forms-name">First name</label>
						<input class="form-input" id="forms-name" type="text" name="name" data-constraints="@Required">
					</div>
				</div>
				<div class="cell-sm-6">
					<div class="form-wrap form-wrap-validation">
						<label class="form-label form-label-outside" for="forms-last-name">Last name</label>
						<input class="form-input" id="forms-last-name" type="text" name="last-name" data-constraints="@Required">
					</div>
				</div>
				<div class="cell-sm-6">
					<div class="form-wrap form-wrap-validation">
						<label class="form-label form-label-outside" for="forms-email">E-mail</label>
						<input class="form-input" id="forms-email" type="email" name="email" data-constraints="@Email @Required">
					</div>
				</div>
				<div class="cell-sm-6">
					<div class="form-wrap form-wrap-validation">
						<label class="form-label form-label-outside" for="forms-phone">Phone</label>
						<input class="form-input" id="forms-phone" type="text" name="phone" data-constraints="@Numeric @Required">
					</div>
				</div>
				<div class="cell-sm-12">
					<div class="form-wrap form-wrap-validation">
						<label class="form-label form-label-outside" for="forms-message">Message</label>
						<textarea class="form-input" id="forms-message" name="message" data-constraints="@Required"></textarea>
					</div>
				</div>
			</div>
			<div class="form-button">
				<button class="button button-primary" type="submit">Submit</button>
			</div>
		</form>
	</div>
</section>