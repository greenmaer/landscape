<?php
use yii\helpers\Url;
?>
<div class="cell-sm-6 cell-md-12">
	<div class="decorative">
		<div class="decorative decorative-md">
			<h5 class="decorative-title">About</h5>
			<div class="sidebar-description">
				<p>Over the last 13 years, GardenLand has created numerous award-winning luxury outdoor living areas. Today we specialize not only in landscape design, but also in other exterior services.</p><a class="post-link" href="<?= Url::to(['site/about']) ?>">Read More</a>
			</div>
		</div>
	</div>
</div>