<section class="section-lg bg-white">
	<div class="decorative decorative-lg">
		<h3 class="decorative-title">Author</h3>
		<article class="blurb-author">
			<div class="unit unit-spacing-md unit-xs-horizontal">
				<div class="unit__left"><img src="/images/news-author-190x190.jpg" alt="" width="190" height="190"/>
				</div>
				<div class="unit__body">
					<h5 class="blurb-author-title">Kevin Wade</h5>
					<p class="blurb-author-position">Blogger</p>
					<p class="blurb-author-text">I am a professional blogger interested in everything taking place in cyberspace. I am running this website and try my best to make it a better place to visit. I post only the articles that are related to the topic and thoroughly analyze all visitors’ comments to cater to their needs better.</p>
				</div>
			</div>
		</article>
	</div>
</section>