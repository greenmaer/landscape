<div class="cell-sm-6 cell-md-12">
	<div class="decorative">
		<div class="decorative decorative-md">
			<h5 class="decorative-title">Archive</h5>
			<ul class="list-marked list-marked-1">
				<li><a href="#">November 2017</a></li>
				<li><a href="#">October 2017</a></li>
				<li><a href="#">September 2017</a></li>
				<li><a href="#">August 2017</a></li>
				<li><a href="#">July 2017</a></li>
				<li><a href="#">June 2017</a></li>
			</ul>
		</div>
	</div>
</div>