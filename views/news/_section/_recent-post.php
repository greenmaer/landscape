
<section class="section-lg bg-white post-single-section">
	<div class="decorative decorative-lg text-center text-xs-left">
		<h3 class="decorative-title">Recent Posts</h3>
		<div class="post-project unit unit-spacing-md unit-xs-horizontal">
			<div class="unit__left"><a href="#"><img src="/images/news-single-3-220x160.jpg" alt="" width="220" height="160"/></a></div>
			<div class="unit__body project-body">
				<div class="project-caption">
					<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
					</p>
					<h6 class="project-title"><a href="#">Triple Victory at Inaugural WA Landscape Design Awards </a></h6>
				</div>
				<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="#">32</a></span><span class="author"><a class="link-gray-dark" href="#">Kevin Wade</a></span></div>
			</div>
		</div>
		<div class="post-project unit unit-spacing-md unit-xs-horizontal">
			<div class="unit__left"><a href="#"><img src="/images/news-single-4-220x160.jpg" alt="" width="220" height="160"/></a></div>
			<div class="unit__body project-body">
				<div class="project-caption">
					<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
					</p>
					<h6 class="project-title"><a href="#">The Right Stone for your Garden Design: Our List of the Top 3 Recommendations</a></h6>
				</div>
				<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="#">32</a></span><span class="author"><a class="link-gray-dark" href="#">Kevin Wade</a></span></div>
			</div>
		</div>
	</div>
</section>