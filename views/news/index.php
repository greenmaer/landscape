<?php
/**
 * @var \yii\web\View $this
 */

use yii\helpers\Url;

$this->title = 'Новости';
?>

<?= $this->render('../_section/_breadcrumbs.php') ?>

<!-- News Classic-->
<section class="section section-sm bg-white">
    <div class="shell shell-fluid">
        <div class="range range-60 range-xl-condensed">
            <div class="cell-md-7 cell-lg-7 cell-xl-6 cell-xl-preffix-1">

				<!-- Blog Classic-->
				<?= $this->render('_section/_articles.php') ?>

            </div>
            <div class="cell-md-5 cell-lg-4 cell-xl-3 cell-lg-preffix-1">
                <!-- Section Blog Modern-->
                <aside class="text-left">
                    <div class="range range-60 range-md-90">

						<!-- RD Search Form-->
						<?= $this->render('_section/_search-form.php') ?>

						<?= $this->render('_section/_archive.php') ?>

						<?= $this->render('_section/_gallery.php') ?>

						<?= $this->render('_section/_categories.php') ?>

						<?= $this->render('_section/_about.php') ?>

						<?= $this->render('_section/_fb-block.php') ?>

                    </div>
                </aside>
            </div>
        </div>
    </div>
</section>