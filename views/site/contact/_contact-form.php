
<section class="section section-sm">
	<div class="shell shell-fluid">
		<div class="range range-60 range-xl-condensed">
			<div class="cell-md-7 cell-lg-6 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Get in Touch</h3>
					<p class="text-block-1">You can contact us any way that is convenient for you. We are available 24/7 via fax or email. You can also use a quick contact form below or visit our office personally. We would be happy to answer your questions.</p>
					<!-- RD Mailform-->
					<form class="rd-mailform text-left" data-form-output="form-output-global" data-form-type="forms" method="post" action="bat/rd-mailform.php">
						<div class="range range-xs-center range-15">
							<div class="cell-sm-6">
								<div class="form-wrap form-wrap-validation">
									<label class="form-label form-label-outside" for="forms-name">First name</label>
									<input class="form-input" id="forms-name" type="text" name="name" data-constraints="@Required">
								</div>
							</div>
							<div class="cell-sm-6">
								<div class="form-wrap form-wrap-validation">
									<label class="form-label form-label-outside" for="forms-last-name">Last name</label>
									<input class="form-input" id="forms-last-name" type="text" name="last-name" data-constraints="@Required">
								</div>
							</div>
							<div class="cell-sm-6">
								<div class="form-wrap form-wrap-validation">
									<label class="form-label form-label-outside" for="forms-email">E-mail</label>
									<input class="form-input" id="forms-email" type="email" name="email" data-constraints="@Email @Required">
								</div>
							</div>
							<div class="cell-sm-6">
								<div class="form-wrap form-wrap-validation">
									<label class="form-label form-label-outside" for="forms-phone">Phone</label>
									<input class="form-input" id="forms-phone" type="text" name="phone" data-constraints="@Numeric @Required">
								</div>
							</div>
							<div class="cell-sm-12">
								<div class="form-wrap form-wrap-validation">
									<label class="form-label form-label-outside" for="forms-message">Message</label>
									<textarea class="form-input" id="forms-message" name="message" data-constraints="@Required"></textarea>
								</div>
							</div>
						</div>
						<div class="form-button">
							<button class="button button-primary" type="submit">Send</button>
						</div>
					</form>
				</div>
			</div>

			<div class="cell-md-4 cell-lg-5 cell-xl-3 cell-lg-preffix-1">
				<div class="contact-box decorative decorative-md">
					<h5 class="decorative-title">Greenfield Office</h5>
					<ul class="list-lg">
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-map-marker"></span></div>
								<div class="unit__body"><a class="link-default" href="#">995 Green Hill St. Greenfield, IN 46140</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-email-outline"></span></div>
								<div class="unit__body"><a class="link-default" href="mailto:#">info@demolink.org</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-phone"></span></div>
								<div class="unit__body"><a class="link-default" href="callto:#">1-800-1234-567</a></div>
							</div>
						</li>
					</ul>
				</div>
				<div class="contact-box decorative decorative-md">
					<h5 class="decorative-title">New York Office</h5>
					<ul class="list-lg">
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-map-marker"></span></div>
								<div class="unit__body"><a class="link-default" href="#">2130 Washington Street, New York, NY 87654-8743 USA</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-email-outline"></span></div>
								<div class="unit__body"><a class="link-default" href="mailto:#">mail@demolink.org</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-phone"></span></div>
								<div class="unit__body"><a class="link-default" href="callto:#">1-800-1234-567</a></div>
							</div>
						</li>
					</ul>
				</div>
				<div class="contact-box decorative decorative-md">
					<h5 class="decorative-title">Whitehall Office</h5>
					<ul class="list-lg">
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-map-marker"></span></div>
								<div class="unit__body"><a class="link-default" href="#">273 Johnson Street, Whitehall, PA 18052</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-email-outline"></span></div>
								<div class="unit__body"><a class="link-default" href="mailto:#">whitehall@demolink.org</a></div>
							</div>
						</li>
						<li>
							<div class="unit unit-horizontal unit-spacing-xxs unit-middle">
								<div class="unit__left"><span class="icon icon-xs icon-primary mdi mdi-phone"></span></div>
								<div class="unit__body"><a class="link-default" href="callto:#">1-800-1234-567</a></div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>