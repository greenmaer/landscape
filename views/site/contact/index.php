<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

?>

<?= $this->render('../../_section/_breadcrumbs.php') ?>

<!-- Get in Touch-->
<?= $this->render('_contact-form.php') ?>

<!-- RD Google Map-->
<?= $this->render('../../_section/_map-locations.php') ?>
