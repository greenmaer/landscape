<?php
/**
 * @var \yii\web\View $this
 */
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */

/* @var $exception Exception|\yii\web\NotFoundHttpException */

use yii\helpers\Html;

Yii::$app->layout = 'mini';

$this->title = $exception->getMessage();
?>

<section>
    <div class="shell">
        <div class="range range-condensed range-xs-center">
            <div class="cell-sm-10 cell-lg-6">
                <h2 class="page-subtitle"><?= $exception->getMessage() ?></h2>
                <p class="text-extra-large page-title"><?= $exception->statusCode ?></p>
                <p class="heading-5 page-description">The page requested couldn't be found - this could be due to a
                    spelling error in the URL or a removed page.</p><a class="button button-white-outline"
                                                                       href="/">Вернуться на главную</a>
            </div>
        </div>
    </div>
</section>
