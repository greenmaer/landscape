<?php
use yii\helpers\Url;
?>
<section class="section section-sm section-sm-bottom-52 bg-gray">
	<div class="shell shell-fluid">
		<div class="range range-xl-condensed range-justify range-30">
			<div class="cell-sm-6 cell-lg-3 cell-xl-2 text-center text-lg-left cell-xl-preffix-1">
				<div class="blurb">
					<div class="icon icon-primary icon-xl fl-line-icon-set-plant10"></div>
					<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Landscape Ideas</a></h5>
					<p class="blurb-content">From finding the best landscape design ideas or inspiration to maintaining your dream yard, the landscaping tips and design ideas will keep you moving.</p>
				</div>
			</div>
			<div class="cell-sm-6 cell-lg-3 cell-xl-2 text-center text-lg-left">
				<div class="blurb">
					<div class="icon icon-primary icon-xl fl-line-icon-set-diamond18"></div>
					<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Architectural Design Coordination</a></h5>
					<p class="blurb-content">Architectural design is typically a multi-staged process, involving a number of designers working together to create a single project that can improve your yard.</p>
				</div>
			</div>
			<div class="cell-sm-6 cell-lg-3 cell-xl-2 text-center text-lg-left">
				<div class="blurb">
					<div class="icon icon-primary icon-xl fl-line-icon-set-cogwheel8"></div>
					<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Landscape Maintenance</a></h5>
					<p class="blurb-content">We will customize a regular service plan just for your yard, focused on highlighting its natural beauty. We will maintain your landscape so you can enjoy satisfaction.</p>
				</div>
			</div>
			<div class="cell-sm-6 cell-lg-3 cell-xl-2 text-center text-lg-left cell-xl-postfix-1">
				<div class="blurb">
					<div class="icon icon-primary icon-xl fl-line-icon-set-square51"></div>
					<h5 class="blurb-title"><a class="text-gray-darker" href="<?= Url::toRoute(['site/about']) ?>">Construction Consultation </a></h5>
					<p class="blurb-content">GardenLand provides comprehensive construction management consulting services that provide the best results for your project, whether it’s commercial or individual.</p>
				</div>
			</div>
		</div>
	</div>
</section>