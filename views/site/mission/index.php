<?php
/**
 * @var \yii\web\View $this
 */
$this->title = 'Наша миссия'
?>
<?= $this->render('../../_section/_breadcrumbs.php') ?>

<!-- Our Approach-->
<?= $this->render('_our-approach.php') ?>

<!-- Services-->
<?= $this->render('_services.php') ?>

<!-- Latest Blog Posts-->
<?= $this->render('../../news/_section/_latest-other.php') ?>
