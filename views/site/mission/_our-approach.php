<?php
use yii\helpers\Url;
?>
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-30 range-xl-condensed">
			<div class="cell-sm-12 cell-md-7 cell-lg-6 cell-xl-5 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Our philosophy</h3>
					<p>Why is it vital to have your home look attractive both inside and out? Well, many would agree that a creative design of the house emphasizes your personality, and in some way, helps you feel better.</p>
					<p>Quite often, people do a huge mistake and pay much attention to the interior leaving the exterior as it is. Such an approach is totally wrong, as the first thing the visitors see is the exterior, and quite often they may develop a wrong opinion of you from that. Fortunately, there is a number of options to deal with this problem.</p><a class="button button-primary" href="<?= Url::toRoute(['site/about']) ?>">Read more about us</a>
				</div>
			</div>
			<div class="cell-sm-12 cell-md-5 cell-xl-4 cell-lg-preffix-1"><img src="/images/mission-1-640x360.jpg" alt="" width="640" height="360"/>
			</div>
		</div>
	</div>
</section>