<?php
/**
 * @var \yii\web\View $this
 */

//$this->title = 'My Yii Application — ' . Yii::$app->name;

?>

    <!-- Swiper-->
<?= $this->render('../_section/_slider.php') ?>

    <!-- Areas of Work-->
<?= $this->render('../_section/_areas-of-work.php') ?>

    <!-- Counters-->
<?= $this->render('../_section/_countries.php') ?>

    <!-- Our Production-->
<?= $this->render('../product/our-product.php') ?>

    <!-- Testimonials-->
<?= $this->render('../_section/_testimonials.php') ?>

    <!-- Latest Company News-->
<?= $this->render('../news/_section/_latest-home.php') ?>

    <!-- RD Google Map-->
<?= $this->render('../_section/_map-locations.php') ?>
