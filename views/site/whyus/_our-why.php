<?php
use yii\helpers\Url;
?>
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-30 range-xl-condensed">
			<div class="cell-sm-12 cell-md-5 cell-xl-4 cell-xl-preffix-1"><img src="/images/social-politics-1-640x360.jpg" alt="" width="640" height="360"/>
			</div>
			<div class="cell-sm-12 cell-md-7 cell-lg-6 cell-xl-5 cell-lg-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Why Our Clients Choose Us</h3>
					<p>Our clients benefit from the peace of mind they receive knowing their landscape and maintenance needs are professionally implemented utilizing the finest in horticultural and environmental practices available thus preserving and increasing their property values and aesthetics. The greatest testimony to the quality of our work is the loyalty of our clients, many of which have been with us for over 40 years. For this, we are truly grateful and we ware going to continue offering high-quality landscape and park design services as well as assistance in landscape restoration.</p><a class="button button-primary" href="<?= Url::toRoute(['site/about']) ?>">VIEW MORE INFORMATION</a>
				</div>
			</div>
		</div>
	</div>
</section>