<?php
use yii\helpers\Url;
?>
<section class="section section-sm bg-gray">
	<div class="shell shell-fluid text-center text-xs-left">
		<div class="range range-60 range-xl-condensed">
			<div class="cell-sm-11 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Events & Awarded Projects</h3>
				</div>
			</div>
			<div class="cell-md-6 cell-xl-4 cell-xl-preffix-1">
				<div class="post-project unit unit-spacing-md unit-xs-horizontal">
					<div class="unit__left"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/social-projects-1-220x160.jpg" alt="" width="220" height="160"/></a></div>
					<div class="unit__body project-body">
						<div class="project-caption width-md-285">
							<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
							</p>
							<h6 class="project-title"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>">San Diego Landscape Design Championship 2016</a></h6>
						</div>
						<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">32</a></span><span class="author"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Gary Welch</a></span></div>
					</div>
				</div>
			</div>
			<div class="cell-md-6 cell-xl-4 cell-xl-preffix-1">
				<div class="post-project unit unit-spacing-md unit-xs-horizontal">
					<div class="unit__left"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/social-projects-2-220x160.jpg" alt="" width="220" height="160"/></a></div>
					<div class="unit__body project-body">
						<div class="project-caption width-md-285">
							<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
							</p>
							<h6 class="project-title"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>">International Lawn and Park Design Exhibition</a></h6>
						</div>
						<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">32</a></span><span class="author"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Gary Welch</a></span></div>
					</div>
				</div>
			</div>
			<div class="cell-md-6 cell-xl-4 cell-xl-preffix-1">
				<div class="post-project unit unit-spacing-md unit-xs-horizontal">
					<div class="unit__left"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/social-projects-3-220x160.jpg" alt="" width="220" height="160"/></a></div>
					<div class="unit__body project-body">
						<div class="project-caption width-md-285">
							<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
							</p>
							<h6 class="project-title"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>">All-American Ecological Exhibition in New York</a></h6>
						</div>
						<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">32</a></span><span class="author"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Gary Welch</a></span></div>
					</div>
				</div>
			</div>
			<div class="cell-md-6 cell-xl-4 cell-xl-preffix-1">
				<div class="post-project unit unit-spacing-md unit-xs-horizontal">
					<div class="unit__left"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>"><img src="/images/social-projects-4-220x160.jpg" alt="" width="220" height="160"/></a></div>
					<div class="unit__body project-body">
						<div class="project-caption width-md-285">
							<p class="project-date"><span class="project-date">July 22, 2017</span> <span class="project-time">at 6:35pm</span>
							</p>
							<h6 class="project-title"><a href="<?= Url::to(['news/view', 'id' => 5]) ?>">International Landscape Design Conference</a></h6>
						</div>
						<div class="project-meta"><span class="comment"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">32</a></span><span class="author"><a class="link-gray-dark" href="<?= Url::to(['news/view', 'id' => 5]) ?>">Gary Welch</a></span></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>