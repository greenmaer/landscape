<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-xl-condensed range-center">
			<div class="cell-sm-12 cell-xl-10">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Our Team</h3>
				</div>
				<div class="range range-40 range-justify">
					<div class="cell-sm-6 cell-md-3 cell-xl-3">
						<div class="thumbnail-custom thumbnail-type-1">
							<figure><img src="/images/team-1-340x340.jpg" alt="" width="340" height="340"/>
							</figure>
							<div class="caption">
								<div class="decorative decorative-md">
									<h5 class="decorative-title caption-title"><a href="#">Adam Hamilton</a></h5>
									<p class="caption-subtitle">CEO</p>
									<p class="caption-description">Adam is a veteran in this landscaping and exterior design business. Starting off in the industry with mowing neighbor’s lawns in high school, his career climaxed when he founded the GardenLand.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 cell-xl-3">
						<div class="thumbnail-custom thumbnail-type-1">
							<figure><img src="/images/team-2-340x340.jpg" alt="" width="340" height="340"/>
							</figure>
							<div class="caption">
								<div class="decorative decorative-md">
									<h5 class="decorative-title caption-title"><a href="#">Philip Spencer</a></h5>
									<p class="caption-subtitle">Head of Innovation</p>
									<p class="caption-description">This far Philip has been devoting himself for more than twenty-five years, successfully practicing and achieving some great feats in architecture, design and construction.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 cell-xl-3">
						<div class="thumbnail-custom thumbnail-type-1">
							<figure><img src="/images/team-3-340x340.jpg" alt="" width="340" height="340"/>
							</figure>
							<div class="caption">
								<div class="decorative decorative-md">
									<h5 class="decorative-title caption-title"><a href="#">David Hoffman</a></h5>
									<p class="caption-subtitle">Managing Director</p>
									<p class="caption-description">David has 15 years of practical experience in all sorts of design at hand, including the interior design works and project management processes.</p>
								</div>
							</div>
						</div>
					</div>
					<div class="cell-sm-6 cell-md-3 cell-xl-3">
						<div class="thumbnail-custom thumbnail-type-1">
							<figure><img src="/images/team-4-340x340.jpg" alt="" width="340" height="340"/>
							</figure>
							<div class="caption">
								<div class="decorative decorative-md">
									<h5 class="decorative-title caption-title"><a href="#">Richard Contreras</a></h5>
									<p class="caption-subtitle">Leading Landscape Designer</p>
									<p class="caption-description">Richard is really into creating spaces that are not just unique visually to each project of his clients, but which also do embrace the individual suggestions from the clients.</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>