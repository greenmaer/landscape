<?php
use yii\helpers\Url;
?>
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-xl-condensed range-30">
			<div class="cell-sm-12 cell-xl-10 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">A Few Words About GardenLand</h3>
					<div class="range range-xl-condensed range-justify range-40">
						<div class="cell-md-6 cell-xl-6">
							<p>Over the last 13 years, GardenLand has created numerous award-winning luxury outdoor living areas. In our time we’ve seen the traditional landscaping and outdoor living evolve from verandas into outdoor spaces.</p>
							<p>It blurred the distinction between inside and outside. GardenLand has also evolved. What started as a garden and landscape design company has expanded to now include renovation, construction, interior design and landscape architecture. This allows us to create designs that seamlessly blend the architecture of your home into stunning outdoor rooms and entertaining spaces.</p><a class="button button-primary" href="<?= Url::toRoute(['site/mission']) ?>">View our mission</a>
						</div>
						<div class="cell-md-5 cell-xl-5">
							<div class="progress-linear">
								<div class="progress-header">
									<div><span>Landscape Ideas</span></div>
									<div><span class="text-bold progress-value">70</span></div>
								</div>
								<div class="progress-bar-linear-wrap">
									<div class="progress-bar-linear"></div>
								</div>
							</div>
							<div class="progress-linear">
								<div class="progress-header">
									<div><span>Landscape Maintenance</span></div>
									<div><span class="text-bold progress-value">92</span></div>
								</div>
								<div class="progress-bar-linear-wrap">
									<div class="progress-bar-linear"></div>
								</div>
							</div>
							<div class="progress-linear">
								<div class="progress-header">
									<div><span>Architectural Design Coordination</span></div>
									<div><span class="text-bold progress-value">31</span></div>
								</div>
								<div class="progress-bar-linear-wrap">
									<div class="progress-bar-linear"></div>
								</div>
							</div>
							<div class="progress-linear">
								<div class="progress-header">
									<div><span>Construction Consultation</span></div>
									<div><span class="text-bold progress-value">65</span></div>
								</div>
								<div class="progress-bar-linear-wrap">
									<div class="progress-bar-linear"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>