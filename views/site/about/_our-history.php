<section class="section section-sm bg-gray">
	<div class="shell shell-fluid">
		<div class="range range-xl-condensed range-30">
			<div class="cell-lg-6 cell-xl-5 cell-xl-preffix-1">
				<div class="image-wrapper-2"><img src="/images/about-1-480x280.jpg" alt="" width="480" height="280"/><img src="/images/about-2-480x280.jpg" alt="" width="480" height="280"/><img src="/images/about-3-480x280.jpg" alt="" width="480" height="280"/>
				</div>
			</div>
			<div class="cell-lg-5 cell-xl-4 cell-lg-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Our History</h3>
					<ul class="timeline">
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2003</p>
							</div>
							<div class="timeline-item-body">
								<p>GardenLand was founded in New York in 2003.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2004</p>
							</div>
							<div class="timeline-item-body">
								<p>We introduced new works and projects based on  the history of landscape design.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2006</p>
							</div>
							<div class="timeline-item-body">
								<p>We opened our first office in Indiana in 2006 to support the increasing demand for landscape design.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2007</p>
							</div>
							<div class="timeline-item-body">
								<p>Our team of designers created their first  commercial landscape.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2008</p>
							</div>
							<div class="timeline-item-body">
								<p>New offices open in Pennsylvania and company employment reaches 100 people for the first time.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2009</p>
							</div>
							<div class="timeline-item-body">
								<p>We start our partnership with external design firms for numerous projects and also offer an in-house architectural design team.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2011</p>
							</div>
							<div class="timeline-item-body">
								<p>GardenLand had more than 40 projects under construction in 2011.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2012</p>
							</div>
							<div class="timeline-item-body">
								<p>Philip Spencer becomes a part of our team.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2013</p>
							</div>
							<div class="timeline-item-body">
								<p>A 10th Anniversary celebration is held with employees, families, and customers at the Company headquarters.</p>
							</div>
						</li>
						<li class="timeline-item">
							<div class="timeline-item-header">
								<p>2016</p>
							</div>
							<div class="timeline-item-body">
								<p>GardenLand has completed an impressive list of projects performed in more than 15 states.</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>