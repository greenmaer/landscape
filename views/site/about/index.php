<?php
/**
 * @var \yii\web\View $this
 */
$this->title = 'О нашей компании'
?>

<?= $this->render('../../_section/_breadcrumbs.php') ?>

<!-- A Few Words About Fabricator-->
<?= $this->render('_fabricator.php') ?>

<!-- Our History-->
<?= $this->render('_our-history.php') ?>

<!--Our Management Team-->
<?= $this->render('_our-team.php') ?>