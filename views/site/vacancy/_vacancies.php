<?php
/**
 * @var \yii\web\View $this
 */
use yii\helpers\Url;
?>
<section class="section section-sm bg-gray">
	<div class="shell shell-fluid">
		<div class="range range-40 range-md-80 range-xl-condensed range-justify">
			<div class="cell-sm-12 cell-xl-10 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Открытые вакансии</h3>
				</div>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3 cell-xl-preffix-1"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Landscape Designer</h6>
					<p class="box-vertical-description">We are looking for an experienced landscape designer in our friendly team.</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Landscape Architect</h6>
					<p class="box-vertical-description">If you are a well-educated and talented Landscape Architect, join our team!</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3 cell-xl-postfix-1"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Sales Manager</h6>
					<p class="box-vertical-description">Become a part of a modern landscape company as a Sales Manager.</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3 cell-xl-preffix-1"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Hard Landscaper</h6>
					<p class="box-vertical-description">GardenLand is looking for a person who has extensive experience in landscaping.</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Stone Layer</h6>
					<p class="box-vertical-description">We are looking for a top class Stonemason for ongoing project in California.</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
			<div class="cell-sm-6 cell-lg-4 cell-xl-3 cell-xl-postfix-1"><a class="box-vertical" href="#">
					<h6 class="box-vertical-header">Estimator</h6>
					<p class="box-vertical-description">We are looking for a professional and skilled person to join our team as an estimator.</p>
					<ul class="list-md list-icon">
						<li><span class="icon icon-xss icon-secondary mdi mdi-map-marker"></span><span>San Diego</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-timelapse"></span><span>Full Time</span></li>
						<li><span class="icon icon-xss icon-secondary mdi mdi-calendar"></span><span class="time">November 8, 2017</span></li>
					</ul></a>
			</div>
		</div>
	</div>
</section>