<?php
/**
 * @var \yii\web\View $this
 */
$this->title = 'Вакансии';
?>
<?= $this->render('../../_section/_breadcrumbs.php') ?>

<!-- Start Your Career with Us-->
<?= $this->render('_start-careers.php') ?>

<!-- Current Vacancies-->
<?= $this->render('_vacancies.php') ?>