<?php
/**
 * @var \yii\web\View $this
 */
?>
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-30 range-xl-condensed">
			<div class="cell-sm-12 cell-md-7 cell-lg-6 cell-xl-4 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Начните свою карьеру с нами!</h3>
					<p>Our success is driven by the quality and dedication of our employees. Become the member of our friendly team! At GardenLand, we seek individuals who share our values as a means of reinforcing and furthering the group's successful philosophy. We are looking for motivated, positive and hardworking individuals to join our team.</p>
					<p>We strive to meet the needs of each employee because our success depends on the satisfaction, effort and commitment of each team member. To this end, we support and energize all employees to continuously improve productivity and exceed guest expectations. Joining GardenLand should not be merely the process of getting a job, but rather, making an occupation lifestyle choice.</p>
				</div>
			</div>
			<div class="cell-sm-12 cell-md-5 cell-xl-5 cell-lg-preffix-1"><img src="/images/careers-1-800x340.jpg" alt="" width="800" height="340"/>
			</div>
		</div>
	</div>
</section>