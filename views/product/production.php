<?php
/**
 * @var \yii\web\View $this
 */
$this->title = 'Наши услуги';
?>
<?= $this->render('../_section/_breadcrumbs.php') ?>

<!-- Production-->
<section class="section section-sm bg-white">
	<div class="shell shell-fluid">
		<div class="range range-40 range-md-90 range-justify range-xl-condensed">
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3 cell-xl-preffix-1">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-1-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Adirondack Black Chair</a></h5>
						<p class="product-description">This Adirondack black chair brings the relaxed lifestyle from the tropical islands to your backyard. Chairs are available in various shapes and colours.</p>
					</div>
				</div>
			</div>
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-2-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Sunbed with Canopy</a></h5>
						<p class="product-description">Our sunbed with canopy, lounging and relaxing outdoor furniture that doesn't leave you fretting over price. Resist the urge to work with this bestseller!</p>
					</div>
				</div>
			</div>
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3 cell-xl-postfix-1">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-3-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Rattan Wicker Chaise</a></h5>
						<p class="product-description">Enjoy relaxing at home with this new wicker chaise that will be adding style to your backyard. The black and white contrast draw attention to its each piece.</p>
					</div>
				</div>
			</div>
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3 cell-xl-preffix-1">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-4-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Wicker Patio Daybed</a></h5>
						<p class="product-description">Harmonize inverse elements with this radically pleasing daybed set. Seven plush throw pillows add even more comfort to this patio daybed.</p>
					</div>
				</div>
			</div>
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-5-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Outdoor Patio Umbrella</a></h5>
						<p class="product-description">This  outdoor umbrella is comfortable sun patio furniture. It can also be used in your garden, near your pool, or on your deck or lawn.</p>
					</div>
				</div>
			</div>
			<div class="cell-sm-6 cell-md-4 cell-lg-6 cell-xl-3 cell-xl-postfix-1">
				<div class="product unit unit-spacing-lg unit-lg-horizontal">
					<div class="unit__left product-media"><a href="/site/single-product.html"><img src="/images/product-6-206x174.jpg" alt="" width="206" height="174"/></a></div>
					<div class="unit__body product-body">
						<h5 class="product-title"><a href="/site/single-product.html">Adirondack White Chair</a></h5>
						<p class="product-description">We made our Adirondack chair out of cedar and finished it with clear wood sealer. It is available in white, battleship gray, and forest green colours.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>