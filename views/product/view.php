<?= $this->render('../_section/_breadcrumbs.php', ['type' => 'product']) ?>

<!-- General Information-->
<section class="section section-sm bg-white">
    <div class="shell shell-fluid">
        <div class="range range-40 range-xl-condensed">
            <div class="cell-md-4 cell-lg-5 cell-xl-4 cell-xl-preffix-1">
                <!-- Slick Carousel-->
                <?= $this->render('_section/_slider.php') ?>
            </div>
            <div class="cell-md-8 cell-lg-7 cell-xl-6">
                <div class="product-single">
                    <?= $this->render('_section/_single-content.php') ?>
                </div>
            </div>
        </div>
    </div>
</section>