<?php
use yii\helpers\Url;
?>
<section class="section section-md bg-white">
	<div class="shell shell-fluid">
		<div class="range range-40 range-xl-condensed">
			<div class="cell-md-5 cell-xl-3 cell-xl-preffix-1">
				<div class="thumbnail-type-3"><img src="/images/index-2-480x400.jpg" alt="" width="480" height="400"/>
					<div class="caption">
						<h5 class="text-bold title">What We Offer</h5>
						<p class="description">If you are looking for high-quality landscape design services, equipment, resources, or consumables, the specialists of our Sales Department will be glad to provide you with any product of your choice.</p><a class="post-link" href="single-product.html">Get in touch</a>
					</div>
				</div>
			</div>
			<div class="cell-md-7 cell-xl-6 cell-xl-preffix-1">
				<div class="decorative decorative-lg">
					<h3 class="decorative-title">Our Production</h3>
					<p>The range of products we manufacture and provide to our customers includes parts for landscape equipment, all kinds of necessary resources and supplies for designing, manufacturing and other landscape spheres.</p>
					<div class="range range-15 range-xl-condensed">
						<div class="cell-sm-4">
							<div class="product-small"><a href="/site/single-product.html"><img src="/images/product-small-1-197x236.jpg" alt="" width="197" height="236"/></a>
								<h5 class="product-title"><a href="/site/single-product.html">Adirondack Black Chair</a></h5>
							</div>
						</div>
						<div class="cell-sm-4">
							<div class="product-small"><a href="/site/single-product.html"><img src="/images/product-small-2-192x237.jpg" alt="" width="192" height="237"/></a>
								<h5 class="product-title"><a href="/site/single-product.html">Adirondack White Chair</a></h5>
							</div>
						</div>
						<div class="cell-sm-4">
							<div class="product-small"><a href="/site/single-product.html"><img src="/images/product-small-3-200x236.jpg" alt="" width="200" height="236"/></a>
								<h5 class="product-title"><a href="/site/single-product.html">Adirondack Brown Chair</a></h5>
							</div>
						</div>
					</div>
					<div class="button-wrap-md text-center text-md-left"><a class="button button-primary" href="<?= Url::toRoute(['product/production']) ?>">View all catalog</a></div>
				</div>
			</div>
		</div>
	</div>
</section>