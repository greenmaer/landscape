
<h3 class="product-title">Adirondack Black Chair</h3>
<p class="product-description">This Adirondack black chair brings the relaxed lifestyle from the tropical islands to your backyard. Chairs are available in various shapes and colours. You can complete the look with a whole set, or simply pick one or two to add to your existing settings.</p>
<div class="group-xl group-price">
	<h5 class="price">From $99.00</h5><a class="button button-primary" href="#">Download price list</a><a class="button button-primary-outline" href="#">Book a consultation</a>
</div>
<div class="tabs-custom-wrap product-tabs">
	<!-- Bootstrap tabs-->
	<div class="tabs-custom tabs-horizontal tabs-modern" id="tabs-1">
		<!-- Nav tabs-->
		<ul class="nav-custom nav-custom-tabs">
			<li class="active"><a href="#tabs-1-1" data-toggle="tab">Additional Information</a></li>
			<li><a href="#tabs-1-2" data-toggle="tab">Video Review</a></li>
			<li><a href="#tabs-1-3" data-toggle="tab">Customer Reviews</a></li>
		</ul>
		<div class="tab-content text-left">
			<div class="tab-pane fade in active" id="tabs-1-1">
				<p>Whether you want to enjoy a cup of coffee with your loved ones while watching the sun set, or read a book alone in a free sunny afternoon, GardenLand provides the perfect product to fit your needs and fit on your budget. Manufactured from Malaysian dark red Meranti wood and treated with teak oill, GardenLand Adirondack chair series is more durable and water resistant.</p>
				<p>All the products are produced and packed 100-percent in Malaysia. It is recommended to treat with suitable wood oil at least once a year to maintain the original appearance.</p>
			</div>
			<div class="tab-pane fade" id="tabs-1-2">
				<div class="entry-video embed-responsive embed-responsive-16by9">
					<iframe width="880" height="494" src="https://www.youtube.com/embed/4wywufv1aSU" allowfullscreen=""></iframe>
				</div>
			</div>
			<div class="tab-pane fade" id="tabs-1-3">
				<div class="post-short">
					<div class="post-meta">
						<ul class="list-divider">
							<li><a href="#">Denise Ray</a></li>
							<li><span class="time">2 days ago</span></li>
						</ul>
					</div>
					<div class="post-body">
						<p class="post-description">Love that chair. I also have had a lifetime bench for several years that I bought on this website. Both still look perfect!</p>
					</div>
				</div>
				<div class="post-short">
					<div class="post-meta">
						<ul class="list-divider">
							<li><a href="#">Lawrence Burns</a></li>
							<li><span class="time">2 days ago</span></li>
						</ul>
					</div>
					<div class="post-body">
						<p class="post-description">I would just like to say thank you for your prompt and effective delivery of this chair, for your friendly and professional support staff! I will recommend your landscape design store to all my friends. Thank you a thousand times!</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>