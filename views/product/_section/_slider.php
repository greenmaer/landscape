<div class="slick-slider carousel-parent slick-slider-product" data-arrows="false" data-loop="false" data-dots="false" data-swipe="true" data-items="1" data-child="#child-carousel" data-for="#child-carousel">
	<div class="item"><img src="/images/product-single-1-638x638.jpg" alt="" width="638" height="638"/>
	</div>
	<div class="item"><img src="/images/product-single-2-638x638.jpg" alt="" width="638" height="638"/>
	</div>
	<div class="item"><img src="/images/product-single-1-638x638.jpg" alt="" width="638" height="638"/>
	</div>
</div>
<div class="slick-slider custom-pagination number-counter" id="child-carousel" data-for=".carousel-parent" data-arrows="false" data-loop="false" data-dots="false" data-swipe="true" data-items="3" data-xs-items="3" data-sm-items="3" data-md-items="3" data-lg-items="3" data-slide-to-scroll="1">
	<div class="item index-counter"></div>
	<div class="item index-counter"></div>
	<div class="item index-counter"></div>
</div>