<?php

namespace app\extended\yii\bootstrap;

use yii\bootstrap\Dropdown as OriginDropdown;
use yii\helpers\Html;

class Dropdown extends OriginDropdown
{
    /**
     * Initializes the widget.
     * If you override this method, make sure you call the parent implementation first.
     */
    public function init()
    {
        if ($this->submenuOptions === null) {
            // copying of [[options]] kept for BC
            // @todo separate [[submenuOptions]] from [[options]] completely before 2.1 release
            $this->submenuOptions = $this->options;
            unset($this->submenuOptions['id']);
        }
        //parent::init();
        //Html::addCssClass($this->options, ['widget' => 'lala']);
    }
}